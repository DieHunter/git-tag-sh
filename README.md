# git-tag-sh

#### 介绍

针对当前项目执行 git 打包操作

#### 安装教程

1.  全局安装：npm install git-tag-sh -g
2.  项目中安装：npm install git-tag-sh

#### 使用说明

1.  版本号：git-tag-sh -V
2.  帮助：git-tag-sh -h
3.  当前目录版本号：git-tag-sh -cv 输出：1.0.0
4.  当前版本打 tag：git-tag-sh 输出：1.0.0
5.  当前版本打正式 tag(增加 tag 前缀)：git-tag-sh -p [string] 输出：v1.0.0
6.  当前版本打 tag 后缀 test：git-tag-sh -s test 输出：1.0.0-test
7.  当前版本打 tag 后缀 alpha(beta, release)：git-tag-sh -a (-b, -r) 输出：1.0.0-alpha(beta, release)
8.  提交 tag 消息：git-tag-sh -m [string]
9.  发布至npm：git-tag-sh -pub
10. 运行npm构建命令：git-tag-sh -bu

#### 参与贡献

1.  Fork 本仓库
2.  Star 本仓库
