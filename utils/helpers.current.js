// 获取当前文件夹路径
const getCurrentDir = () => process.cwd();
// 获取文件内容
const getFile = (dir, path) => require(`${dir}${path}`);
// 获取package文件内容
exports.currentPackage = getFile(getCurrentDir(), "/package.json");
exports.getFile = getFile;
exports.getCurrentDir = getCurrentDir;
