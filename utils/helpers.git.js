const { shell } = require("./helpers.shell");
const { catchAwait } = require("utils-lib-js");
// git操作
exports.git = {
  // 校验有无git
  hasGit: async () => catchAwait(shell("git --version", false)),
  // 检测标签是否已存在
  hasTag: async (tag) => {
    const [err, tags] = await catchAwait(shell(`git tag -l "${tag}"`, false));
    const len = tags?.length > 0;
    if (err ?? len) return err ?? "标签已存在";
  },
  tag: async (tag, message) => {
    const [err] = await catchAwait(
      shell(`git tag -a ${tag} -m "${message}"`, false)
    );
    if (err) return err;
  },
  push: async (tag) => {
    const [err] = await catchAwait(shell(`git push origin ${tag}`, false));
    const succ = err.includes(tag);
    if (err && !succ) return err;
  },
};
