exports.options = [
  {
    flags: "-cv, -curVer",
    description:
      "获取当前目录下程序版本号（Get the program version number in the current directory）",
  },
  {
    flags: "-p, -production [string]",
    description:
      "是否打'生产环境'标签（Whether to label the 'production' environment）",
  },
  {
    flags: "-s, -suffix [string]",
    description: "增加标签后缀（Add tag suffix）",
  },
  {
    flags: "-m, -message [string]",
    description: "增加标签提交信息（Add tag submission information）",
  },
  {
    flags: "-a, -alpha",
    description:
      "增加'alpha'后缀，标识为软件开发初期包（Add the suffix 'alpha' to identify the initial package of software development）",
  },
  {
    flags: "-b, -beta",
    description:
      "增加'beta'后缀，标识为软件开发中期包（Add the suffix 'beta' and mark it as software development interim package）",
  },
  {
    flags: "-r, -release",
    description:
      "增加'release'后缀，标识为软件开发完成包（Add the suffix 'release' to identify the software development completion package）",
  },
  {
    flags: "-pub, -publish",
    description: "发布至npm中（Publish to npm）",
  },
  {
    flags: "-bu, -build",
    description: "运行构建命令（Run build command）",
  },
];
exports.prodKey = "v"; // 生产环境标识
exports.splitKey = "-"; // tag后缀分隔符
