// 缩写一下reject函数
const rej = (err = "") => Promise.reject(err);
// 缩写一下resolve函数
const res = (result = "") => Promise.resolve(result);
// 对象转数组
const Obj2Arr = (obj = {}) => Reflect.ownKeys(obj).map((it) => obj[it]);
// 首字母转小写
const first2Lower = (str = "") =>
  str.substring(0, 1).toLowerCase() + str.substring(1);

// 计数器,计算对象中有几个whiteList（白名单）的值（whiteList代表校验哪些key），maxCont表示最大计算到几个为止提前跳出循环
function countArgs(opts, whiteList = [], maxCont, count = 0) {
  for (const it of whiteList) {
    if (maxCont === count) return maxCont;
    !!opts[it] && count++;
  }
  return count;
}
module.exports = {
  res,
  rej,
  Obj2Arr,
  first2Lower,
  countArgs,
};
